unit FMain;

interface

uses
  SysUtils, Windows, Forms, Classes, Controls, StdCtrls,
  ComCtrls, Dialogs, Grids, Graphics, UModel, Vcl.ExtCtrls;

type
  // ������� �����.
  TMainForm = class(TForm)
    FieldGrid: TStringGrid;
    OpenResDialog: TOpenDialog;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    FieldSizeEdit: TEdit;
    CreateFieldButton: TButton;
    GroupBox2: TGroupBox;
    NormalizeCheckBox: TCheckBox;
    OpenResButton: TButton;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    StepCountEdit: TEdit;
    StartButton: TButton;
    Label5: TLabel;
    ResMinEdit: TEdit;
    Label6: TLabel;
    ResMaxEdit: TEdit;
    Label7: TLabel;
    GrowthConsumptionEdit: TEdit;
    Label8: TLabel;
    LivingConsumptionEdit: TEdit;
    JmpMemCheckBox: TCheckBox;
    NoTorCheckBox: TCheckBox;
    RemoveAllButton: TButton;
    StatusBar: TStatusBar;
    UpDown1: TUpDown;
    Label9: TLabel;
    ScaleBar: TTrackBar;
    GroupBox4: TGroupBox;
    pcMushrooms: TPageControl;
    tsCurrentMushroom: TTabSheet;
    Panel1: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    Label3: TLabel;
    SeedXEdit: TEdit;
    SeedYEdit: TEdit;
    SeedXYButton: TButton;
    bRandomXY: TButton;
    tsRandomMushrooms: TTabSheet;
    Panel2: TPanel;
    Label2: TLabel;
    SeedCountEdit: TEdit;
    SeedButton: TButton;
    bShowLog: TButton;
    Result: TButton;
    ed_velocity: TEdit;
    Lb_Velocity: TLabel;
    bLoadTestData: TButton;
    Label12: TLabel;
    updownStasisAge: TUpDown;
    StasisAgeEdit: TEdit;
    cbShowMashrooms: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CreateFieldButtonClick(Sender: TObject);
    procedure SeedButtonClick(Sender: TObject);
    procedure SeedXYButtonClick(Sender: TObject);
    procedure StartButtonClick(Sender: TObject);
    procedure RemoveAllButtonClick(Sender: TObject);
    procedure OpenResButtonClick(Sender: TObject);
    procedure FieldGridDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    function  getMatherLenght(): integer; // ������� ������� �������� matherLenght
    procedure UpDown1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    // ������� ��������� ������������� ���� ������
    function MushroomCellMotherType(X, Y: Integer): string;
    function MushroomCellFamilyId(X, Y: Integer): integer;
    function MushroomCellClanId(X, Y: Integer): integer;
    procedure ScaleBarChange(Sender: TObject);
    procedure ResultClick(Sender: TObject);
    procedure JmpMemCheckBoxClick(Sender: TObject);
    procedure FieldGridMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure bRandomXYClick(Sender: TObject);
    procedure bShowLogClick(Sender: TObject);
    procedure bLoadTestDataClick(Sender: TObject);
    procedure cbShowMashroomsClick(Sender: TObject);

  private
    FModel: TModel;  // ������
    matherLenght: integer;  // ����������������� ����� � ����� �����������, ����� ���� ����� ��������� ����������� ������
  private
    // ��������� �������� ������ �������� ���������� �� ����� ����� �� �����. ����
    // ��������� �������� �����������, ������� ������� ��������� � ������ False.
    function UpdateLimits: Boolean;
    //���� ����� �� ���������� ������� �������� �������
    function UpdateConsumption: Boolean;
    function UpdateVelocity: Boolean;
    // ��������� ������ ���������.
    procedure UpdateStatusBar;
  end;

var
  MainForm: TMainForm;

implementation
uses ULog, UCell;

{$R *.dfm}

//----------------------------------------------------------------------------------------------------------------------
function TMainForm.getMatherLenght(): integer;
begin
  result := matherLenght;
end;

procedure TMainForm.bLoadTestDataClick(Sender: TObject);
const
  fileName = 'matrix_2D_154_grad.csv';
var
  Loaded: boolean;
  Path: string;
begin
  FieldSizeEdit.Text := '154';
  CreateFieldButton.Click;
  Path := ExtractFilePath(Application.ExeName);
  Loaded := FModel.Field.LoadResourcesFromFile(Path + fileName, NormalizeCheckBox.Checked);
  // ��������� ����.
  FieldGrid.Refresh;
  // ��������� ������ �������.
  if Loaded then begin
    StatusBar.Panels[0].Text := '������� ���������' ;
    if FModel.Seed(90, 90) then begin
    StartButton.Enabled := True;


  end;
  end else begin
    StatusBar.Panels[0].Text := '������ �������� ��������';
  end;
  StatusBar.Panels[2].Text := '';
end;

procedure TMainForm.bRandomXYClick(Sender: TObject);
var
  X, Y: integer;
begin
  FModel.GetRandomCoordinate(X, Y);
  SeedXEdit.Text := IntToStr(X + 1);
  SeedYEdit.Text := IntToStr(Y + 1);
end;

procedure TMainForm.bShowLogClick(Sender: TObject);
begin
  LogForm.Show;
end;

procedure TMainForm.cbShowMashroomsClick(Sender: TObject);
begin
  FieldGrid.Repaint;
end;

procedure TMainForm.CreateFieldButtonClick(Sender: TObject);
var
  Size, E: Integer;
begin
  // ������������ ������, ��������� � ����
  // ����� "����������� ����" � ����� �����.
  Val(FieldSizeEdit.Text, Size, E);
  // ��������� ������������ �����.
  if (E <> 0) or (Size < 1) then begin
    Application.MessageBox('�������� ���� "����������� ����" �����������',
      '������ � ���������� �����', MB_ICONERROR or MB_OK);
    Exit;
  end;

  // ������ ��� ������ ����� ������� ���� (� ������ ��� ������).
  CreateFieldButton.Caption := '�������� ����';

  // ���������������� ��������� ��. �����.
  Randomize;

  // ����� �������� ���� ���������
  // ������ ������ ����� ���������.
  SeedButton.Enabled := True;
  SeedXYButton.Enabled := True;
  OpenResButton.Enabled := True;
  RemoveAllButton.Enabled := True;
  // � ������ "������" ��������.
  StartButton.Enabled := False;

  // �������� ����������� ����� (����).
  FieldGrid.RowCount := Size;
  FieldGrid.ColCount := Size;
  FieldGrid.Visible := True;

  // ������� ���� ������� �������.
  FModel.CreateField(Size, Size);
  // ���������� ��������� ������.
  FModel.Reset;

  // ��������� ���� (����).
  FieldGrid.Refresh;

  // ��������� ������ �������.
  StatusBar.Panels[0].Text := '���� �������';
  StatusBar.Panels[1].Text := Format('������ ����: %dx%d', [Size, Size]);


end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.FieldGridDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  Color: Byte;
  text: String;
begin
  // ���������, ���� �� � ������ ����.
  if FModel.Field[Acol, Arow].Exists and cbShowMashrooms.Checked then begin
    case FModel.Field[Acol, Arow].CellType of
      SIMPLE_CELL: FieldGrid.Canvas.Brush.Color := clRed;     // ������� ������
      MOTHER_CELL: FieldGrid.Canvas.Brush.Color := clFuchsia; // ����������� ������
      DAUGHTER_CELL: FieldGrid.Canvas.Brush.Color := clBlue;    // �������� ������
      JUMPING_CELL: FieldGrid.Canvas.Brush.Color := clAqua;    // "���������" ����
    end;
    // ����������� ������ ��������� ������.
    FieldGrid.Canvas.FillRect(Rect);
    if FModel.isStasis(ACol, ARow) then begin
      FieldGrid.Canvas.Font.Color := clWhite;
      text := 'X';
      DrawText(FieldGrid.Canvas.Handle, PChar(text), Length(text), Rect, DT_CENTER or DT_VCENTER or DT_SINGLELINE);
    end;
  end else begin
    // ���� � ������ ��� �����, �� ��������� �������� ������������� ����� (�� 0
    // �� 195). ��� ������ �������� � ������, ��� ���� �������� �������������.
    Color := Round(195 * FModel.Field[Acol, Arow].Source);
    // ���� � ������ �������� ��� (������������� �����
    // ����� 0), �� �������� ��� ������ ����� ����.
    if Color = 0 then begin
      FieldGrid.Canvas.Brush.Color := clWhite
    end else begin
      // ���� ������� ����, �� �������� ��� ������ ������
      // ����: ��� ������ ��������, ��� ���� ����� �� ����.
      FieldGrid.Canvas.Brush.Color := RGB(60 + Color, 60 + Color, 0);
    end;
    // ����������� ������ ��������� ������.
    FieldGrid.Canvas.FillRect(Rect);
  end;

end;

procedure TMainForm.FieldGridMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  ACol, ARow: integer;
  Info, info2: String;
  Resources, ResoursesIn: Single;
  fam: TPoint;
  Lengtch: integer;
begin
  FieldGrid.MouseToCell(X, Y, ACol, ARow);
  Info := '�����';
  info2 := ''; // inttostr(FModel.Field[ACol, ARow].DickLength);
  Resources := 0.0;
  ResoursesIn := 0.0;
  StatusBar.Panels[2].Text := '';
  // ���� ���������� ���������, ��������� ���������� �������� � ������.
  if (ACol >= 0) and (ACol < FModel.Field.Width) and (ARow >= 0) and (ARow < FModel.Field.Height) then begin
    Resources := FModel.Field[ACol, ARow].Source;
    ResoursesIn := FModel.Field[ACol, ARow].SourceIn ;
    Lengtch := FModel.Field[ACol, ARow].DickLength ;
    if (FModel.Field[ACol, ARow].CellType = MOTHER_CELL) OR
          (FModel.Field[ACol, ARow].CellType = DAUGHTER_CELL) then begin
      fam.X := FModel.Field[ACol, ARow].RelatedCellCoord.X + 1;
      fam.Y := FModel.Field[ACol, ARow].RelatedCellCoord.Y + 1;
      info2 := info2 + '����� � [' + inttostr(fam.X) + ',' + inttostr(fam.y) + ']';
    end;

    if FModel.Field[ACol, ARow].Exists then begin
      Info := Format('����: ����-� ����� %d', [Ord(FModel.Field[ACol, ARow].Direction)]);
    end;
    // ������� � ������ ������� ���������� ������, ���-�� �������� � ���� �� � ��� ����.
    StatusBar.Panels[2].Text := Format('������ [%d,%d]: ���. %.4f; ���.��. %.4f; %s %s %s, �����: %d, ����: %d',
                                  [ACol + 1, ARow + 1, Resources, ResoursesIn, Info, Info2,
                                  MushroomCellMotherType(ACol, ARow), MushroomCellFamilyId(ACol, ARow), MushroomCellClanId(ACol, ARow),
                                  '������ ��� ', Lengtch]);
  end;



end;


//----------------
function TMainForm.MushroomCellMotherType(X, Y: Integer): string;
begin
  result := FModel.Field[X, Y].MotherType;
end;

function TMainForm.MushroomCellFamilyId(X, Y: Integer): integer;
begin
  result := FModel.Field[X, Y].FamilyId;
end;

function TMainForm.MushroomCellClanId(X, Y: Integer): integer;
begin
  result := FModel.Field[X, Y].ClanId;
end;


procedure TMainForm.JmpMemCheckBoxClick(Sender: TObject);
begin

end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.FormCreate(Sender: TObject);
begin
  // ������� ������ ������.
  startLogForm;
  FModel := TModel.Create(LogForm);
  // ��������� ��������
  matherLenght := 4;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.FormDestroy(Sender: TObject);
begin
  // ���������� ������ ������.
  FreeAndNil(FModel);
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.OpenResButtonClick(Sender: TObject);
var
  Normalize, Loaded: Boolean;
begin
  // ��������� ���� ������ �����.
  OpenResDialog.Title := '�������� ����� �������� �� �����...';
  // ��������� ���� ������ �����.
  if OpenResDialog.Execute then begin
    Normalize := NormalizeCheckBox.Checked;
    Loaded := FModel.Field.LoadResourcesFromFile(OpenResDialog.FileName, Normalize);
    // ��������� ����.
    FieldGrid.Refresh;
    // ��������� ������ �������.
    if Loaded then begin
      StatusBar.Panels[0].Text := '������� ���������'
    end else begin
      StatusBar.Panels[0].Text := '������ �������� ��������';
    end;
    StatusBar.Panels[2].Text := '';
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.RemoveAllButtonClick(Sender: TObject);
var
  X, Y: Integer;
begin
  // ��������� ������ "������".
  StartButton.Enabled := False;
  LogForm.ClearLog;
  // ������� � ���� ��� �����.
  for X := 0 to FModel.Field.Width - 1 do begin
    for Y := 0 to FModel.Field.Height - 1 do begin
      FModel.Field.Kill(X, Y, 0);
    end;
  end;
  // ��������� ������.
  FModel.Reset;
  // ��������� ���� (����).
  FieldGrid.Refresh;
  // ��������� ������ �������.
  UpdateStatusBar;
end;


procedure TMainForm.ResultClick(Sender: TObject);
begin
  ShowMessage('�������� ' + FloatToStr(Fmodel.m_transp_summ_step)
      + ' ��������� (��������� �� ����� ����� �����) ' +#13#10
      + ' ���������� �������� ������ ' + IntToStr(FModel.modelCellsCount) +#13#10
      + ' ������� ������ ����� ����� ������� ������ ' + FloatToStr(FModel.mean_substrat_value));
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.ScaleBarChange(Sender: TObject);
begin
  FieldGrid.DefaultColWidth  := ScaleBar.Position * 2;
  FieldGrid.DefaultRowHeight := ScaleBar.Position * 2;
  FieldGrid.Refresh;
end;

procedure TMainForm.SeedButtonClick(Sender: TObject);
var
  Count: Integer;
begin
  // ������������ ������, ��������� � ����
  // ����� "����� ������" � ����� �����.
  Count := StrToIntDef(SeedCountEdit.Text, 0);
  // ��������� ������������ �����.
  if (Count < 0) or (Count >= FModel.Field.Width * FModel.Field.Height) then begin
    Application.MessageBox('�������� ���� "����� ������" �����������',
      '������ � ���������� �����', MB_ICONERROR or MB_OK);
    Exit;
  end;

  // ��������� �������� ������ ��������.
  if not UpdateLimits then begin
    Application.MessageBox('������ ����� ��� �����-�� ������, ��� ��� ��������� �������������',
      '����������� ������', MB_ICONERROR or MB_OK);
    Exit;
  end;

  // �������� Count (��� ������) ������. ���� ����� ������� ���� ��
  // 1 ������, ��������� ������ "������". ����� ������� ���������.
  if FModel.Seed(Count) then begin
    StartButton.Enabled := True
  end else begin
    Application.MessageBox('����� �� ���� �������: �� ���� ��� ���������� ������',
      '����� ������', MB_ICONINFORMATION or MB_OK);
  end;

  // ��������� ���� (����).
  FieldGrid.Refresh;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.SeedXYButtonClick(Sender: TObject);
var
  X, Y: Integer;
begin
  // ������������ ������, ��������� � ����
  // ����� "���������� ������" � ����� �����.
  X := StrToIntDef(SeedXEdit.Text, 0);
  Y := StrToIntDef(SeedYEdit.Text, 0);
  // ��������� ������������ ���������.
  if (X < 1) or (Y < 1) or (X > FModel.Field.Width) or (Y > FModel.Field.Height) then begin
    Application.MessageBox('�������� � ����� "���������� ������" �����������',
      '������ � ���������� �����', MB_ICONERROR or MB_OK);
    Exit;
  end;

  // ��������� �������� ������ ��������.
  if not UpdateLimits then begin
    Exit;
  end;

  // �������� ������ X,Y. ���� ������ �����
  // �������, �� ��������� ������ "������".
  if FModel.Seed(X-1, Y-1) then begin
    StartButton.Enabled := True;
  end;

  // ��������� ���� (����).
  FieldGrid.Refresh;
  LogForm.AddLog('������� � ' + IntToStr(X) + ', ' + IntToStr(Y));
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.StartButtonClick(Sender: TObject);
var
  Count, i: Integer;
begin
  // ������������ ��������� � ���� �����
  // "���������� �����" ������ � ����� �����.
  Count := StrToIntDef(StepCountEdit.Text, 0);
  // ��������� ������������ �����.
  if Count < 1 then begin
    Application.MessageBox('�������� ���� "���������� �����" �����������',
      '������ � ���������� �����', MB_ICONERROR or MB_OK);
    Exit;
  end;

  // ��������� �������� ������ ��������.
  if not UpdateLimits then begin
    Exit;
  end;
  // �������� �������� ������� ��������.
  if not UpdateConsumption then begin
    Exit;
  end;
  // ��������� ���� ������� ������ ��� ������.
  FModel.ClearMemAfterJump := not JmpMemCheckBox.Checked;

  // ������ ������� �������
  FModel.setStasisAge(updownStasisAge.Position);

  // ��������� ������ "������".
  StartButton.Enabled := False;

  // ���������� ��������� ���������� �����.
  for i := 1 to Count do begin
    // ���������� ��������� ���.
    FModel.DoStep(NoTorCheckBox.Checked);
    // �������������� ���� � ����.
    FieldGrid.Refresh;
    // ��������� ������ �������.
    UpdateStatusBar;
    // ������������ ����������� ������� � ������� ����
    Application.ProcessMessages;
  end;

  // ����� ��������� ������.
  StartButton.Enabled := True;
end;

//----------------------------------------------------------------------------------------------------------------------
function TMainForm.UpdateLimits: Boolean;
begin
  // ��������� �������� ������ ��������.
  Result := FModel.Limits.SetLimits(ResMinEdit.Text, ResMaxEdit.Text);
  // ������� ��������� � ������ �������������� ������.
  if not Result then begin
    Application.MessageBox('�������� ������ �������� �����������',
      '������ � ���������� �����', MB_ICONERROR or MB_OK);
  end;
end;

function TMainForm.UpdateVelocity: Boolean;
begin
  // ��������� �������� ������ ��������.
  Result := FModel.Limits.SetLimits(ResMinEdit.Text, ResMaxEdit.Text);
  // ������� ��������� � ������ �������������� ������.
  if not Result then begin
    Application.MessageBox('�������� ������ �������� �����������',
      '������ � ���������� �����', MB_ICONERROR or MB_OK);
  end;
end;

function TMainForm.UpdateConsumption: Boolean;
begin
  // ��������� �������� ������ ��������.
  Result := FModel.Limits.SetConsumption(GrowthConsumptionEdit.Text, LivingConsumptionEdit.Text);
  // ������� ��������� � ������ �������������� ������.
  if not Result then begin
    Application.MessageBox('�������� ������ �������� �����������',
      '������ � ���������� �����', MB_ICONERROR or MB_OK);
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.UpdateStatusBar;
begin
  StatusBar.Panels[0].Text := '��� �������������: ' + IntToStr(FModel.Step);
  StatusBar.Panels[2].Text := '';
end;

procedure TMainForm.UpDown1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  matherLenght := updown1.Position;
  label9.Caption := '���������� ����� �� �������� ������: ' + inttostr(updown1.Position);
end;

end.
