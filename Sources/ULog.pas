unit ULog;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TLogForm = class(TForm)
    mLog: TMemo;
    bClear: TButton;
    bSave: TButton;
    SaveDialog1: TSaveDialog;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure bClearClick(Sender: TObject);
    procedure bSaveClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    lines: TStringList;
  public
    { Public declarations }
    procedure ClearLog;               //�������� ���
    procedure AddLog(line: string);   //�������� ������ � ���
  end;

var
  LogForm: TLogForm;

procedure startLogForm;     //��������� �������� ����� ��� ����������� ����

implementation

{$R *.dfm}

//��������� �������� ����� ��� ����������� ����
procedure startLogForm;
begin
  LogForm := TLogForm.Create(Application);         //������� ����� ��� ����������� ����
end;

//������� ������ ��������
procedure TLogForm.bClearClick(Sender: TObject);
begin
  ClearLog;       //�������� ������� ��� ������� ����
end;

//������� ������� ������ ���������
procedure TLogForm.bSaveClick(Sender: TObject);
begin
  if SaveDialog1.Execute then begin        //���� ������������ ������ ��� �����
    try
      mLog.Lines.SaveToFile(SaveDialog1.FileName);       //��������� ������ �� ������� mLog � ����
    except
      //� ������ ������ �������� ���������:
      Application.MessageBox(PChar('������ ��� ���������� ���� � ����: '#13#10 + SaveDialog1.FileName),
                            '������', MB_ICONERROR or  MB_OK);
      exit;
    end;
    //���� ����� ������ ��� ����������, �� ���������� �� ������ �� ���� ������
    //�������� ��������� � ������ ������:
    Application.MessageBox(PChar('��� ������� �������� � ����: '#13#10 + SaveDialog1.FileName),
                            '����������', MB_ICONINFORMATION or  MB_OK);
  end;
end;

//������� ��� �������� �����
procedure TLogForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caHide;         //������ ���� ���� �������, �������� �����
end;

//������� ��� �������� �����
procedure TLogForm.FormCreate(Sender: TObject);
begin
  lines := TStringList.Create;      //������� �����, � ������� ����� ������ ������, ����� ����� ������
  ClearLog;
end;

//������� ��� ����������� �����
procedure TLogForm.FormDestroy(Sender: TObject);
begin
  FreeAndNil(lines);
  inherited;
end;

//������� ��� ����������� �����
procedure TLogForm.FormShow(Sender: TObject);
var
  i: integer;
begin
  //��������� ��� ����������� ����� ������
  for i := 0 to lines.Count-1 do begin
    mLog.Lines.Add(lines[i]);
  end;
  lines.Clear;  //������� ����������� ����� ������
end;

//��������� ��� ������� ����
procedure TLogForm.ClearLog;
begin
  mLog.Lines.Clear;         //������� ���� TMemo, � ������� ���������� ���
  lines.Clear;              //������� ����� �����, ������� ������������, ����� ����� ������
end;

procedure TLogForm.AddLog(line: string);
begin
  //���� ����� �������, �� ��������� ������ � ��� �����
  if Visible then begin
    mLog.Lines.Add(line);
  end else begin
    //����� ����������� ������ � ����� ��������� �����, ������� �� � ���
    lines.Add(line);
  end;
end;

end.
